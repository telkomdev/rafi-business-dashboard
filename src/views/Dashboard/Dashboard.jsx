import React, { Component } from "react";
import ChartistGraph from "react-chartist";
import { Grid, Row, Col } from "react-bootstrap";
import { ResponsiveLine } from '@nivo/line';

import { Card } from "components/Card/Card.jsx";
import { StatsCard } from "components/StatsCard/StatsCard.jsx";
import { Tasks } from "components/Tasks/Tasks.jsx";
import {
  dataPie,
  legendPie,
  dataSales,
  optionsSales,
  responsiveSales,
  legendSales,
  dataBar,
  optionsBar,
  responsiveBar,
  activeUsersAndroid,
  activeUsersIos,
  drink,
  legendBar
} from "variables/Variables.jsx";
import TotalUser from '../../containers/TotalUser';
import TotalPost from '../../containers/TotalPost';
import TotalDownload from '../../containers/TotalDownload';

class Dashboard extends Component {
  createLegend(json) {
    var legend = [];
    for (var i = 0; i < json["names"].length; i++) {
      var type = "fa fa-circle text-" + json["types"][i];
      legend.push(<i className={type} key={i} />);
      legend.push(" ");
      legend.push(json["names"][i]);
    }
    return legend;
  }
  render() {
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col lg={4} sm={6}>
              <TotalPost />
            </Col>
            <Col lg={4} sm={6}>
              <TotalUser />
            </Col>
            <Col lg={4} sm={6}>
              <TotalDownload />
            </Col>
            {/*<Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="fa fa-twitter text-info" />}
                statsText="Followers"
                statsValue="+45"
                statsIcon={<i className="fa fa-refresh" />}
                statsIconText="Updated now"
              />
            </Col>*/}
          </Row>
          <Row>
            {/*<Col md={6}>
              <Card
                statsIcon="fa fa-history"
                id="chartHours"
                title="Users Behavior"
                category="24 Hours performance"
                stats="Updated 3 minutes ago"
                content={
                  <div className="ct-chart">
                    <ChartistGraph
                      data={dataSales}
                      type="Line"
                      options={optionsSales}
                      responsiveOptions={responsiveSales}
                    />
                  </div>
                }
                legend={
                  <div className="legend">{this.createLegend(legendSales)}</div>
                }
              />
              </Col>*/}
            <Col md={6}>
              <Card
                statsIcon="fa fa-history"
                id="chartHours"
                title="Active Android User"
                category="monthly performance"
                stats="Updated 3 minutes ago"
                content={
                  <div className="ct-chart">
                    <ResponsiveLine
                      data={activeUsersAndroid}
                      margin={{
                        "top": 5,
                        "right": 10,
                        "bottom": 5,
                        "left": 6
                      }}
                      minY="auto"
                      stacked={true}
                      curve="natural"
                      axisBottom={{
                        "orient": "bottom",
                        "tickSize": 5,
                        "tickPadding": 5,
                        "tickRotation": 0,
                        "legend": "country code",
                        "legendOffset": 36,
                        "legendPosition": "center"
                      }}
                      axisLeft={{
                        "orient": "left",
                        "tickSize": 5,
                        "tickPadding": 5,
                        "tickRotation": 0,
                        "legend": "count",
                        "legendOffset": -40,
                        "legendPosition": "center"
                      }}
                      colors="set3"
                      enableDots={false}
                      dotColor="inherit:darker(0.3)"
                      dotBorderColor="#ffffff"
                      enableDotLabel={true}
                      dotLabel="y"
                      dotLabelYOffset={-12}
                      animate={true}
                      motionStiffness={90}
                      motionDamping={15}
                      legends={[
                        {
                          "anchor": "bottom-right",
                          "direction": "column",
                          "translateX": 100,
                          "itemWidth": 80,
                          "itemHeight": 20,
                          "symbolSize": 12,
                          "symbolShape": "circle"
                        }
                      ]}
                    />
                  </div>
                }
              />
            </Col>
            <Col md={6}>
              <Card
                statsIcon="fa fa-history"
                id="chartHours"
                title="Active IOS User"
                category="monthly performance"
                stats="Updated 3 minutes ago"
                content={
                  <div className="ct-chart">
                    <ResponsiveLine
                      data={activeUsersIos}
                      margin={{
                        "top": 5,
                        "right": 10,
                        "bottom": 5,
                        "left": 6
                      }}
                      minY="auto"
                      stacked={true}
                      curve="natural"
                      axisBottom={{
                        "orient": "bottom",
                        "tickSize": 5,
                        "tickPadding": 5,
                        "tickRotation": 0,
                        "legend": "country code",
                        "legendOffset": 36,
                        "legendPosition": "center"
                      }}
                      axisLeft={{
                        "orient": "left",
                        "tickSize": 5,
                        "tickPadding": 5,
                        "tickRotation": 0,
                        "legend": "count",
                        "legendOffset": -40,
                        "legendPosition": "center"
                      }}
                      colors="set3"
                      enableDots={false}
                      dotColor="inherit:darker(0.3)"
                      dotBorderColor="#ffffff"
                      enableDotLabel={true}
                      dotLabel="y"
                      dotLabelYOffset={-12}
                      animate={true}
                      motionStiffness={90}
                      motionDamping={15}
                      legends={[
                        {
                          "anchor": "bottom-right",
                          "direction": "column",
                          "translateX": 100,
                          "itemWidth": 80,
                          "itemHeight": 20,
                          "symbolSize": 12,
                          "symbolShape": "circle"
                        }
                      ]}
                    />
                  </div>
                }
              />
            </Col>
          </Row>
          {/*<Row>
            <Col md={8}>
              <Card
                statsIcon="fa fa-clock-o"
                title="Email Statistics"
                category="Last Campaign Performance"
                stats="Campaign sent 2 days ago"
                content={
                  <div
                    id="chartPreferences"
                    className="ct-chart ct-perfect-fourth"
                  >
                    <ChartistGraph data={dataPie} type="Pie" />
                  </div>
                }
                legend={
                  <div className="legend">{this.createLegend(legendPie)}</div>
                }
              />
            </Col>
              </Row>*/}
          {/*<Row>
            <Col md={6}>
              <Card
                id="chartActivity"
                title="2014 Sales"
                category="All products including Taxes"
                stats="Data information certified"
                statsIcon="fa fa-check"
                content={
                  <div className="ct-chart">
                    <ChartistGraph
                      data={dataBar}
                      type="Bar"
                      options={optionsBar}
                      responsiveOptions={responsiveBar}
                    />
                  </div>
                }
                legend={
                  <div className="legend">{this.createLegend(legendBar)}</div>
                }
              />
            </Col>

            <Col md={6}>
              <Card
                title="Tasks"
                category="Backend development"
                stats="Updated 3 minutes ago"
                statsIcon="fa fa-history"
                content={
                  <div className="table-full-width">
                    <table className="table">
                      <Tasks />
                    </table>
                  </div>
                }
              />
            </Col>
              </Row>*/}
        </Grid>
      </div>
    );
  }
}

export default Dashboard;
