import {
    combineReducers
} from 'redux';
import {
    routerReducer
} from 'react-router-redux';

import * as userReducers from './user';
import * as postReducers from './post';


export default combineReducers(Object.assign({},
  userReducers,
  postReducers,
	{
	  routing: routerReducer
	},
));