import { request, access_token } from '../api'

export const restGetTotalUser = async () => {

    return request.get(
        'users/total', {
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Basic ' + access_token
            }
        }
    )
}
