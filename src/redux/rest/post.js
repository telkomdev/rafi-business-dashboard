import { request, access_token } from '../api'

export const restGetTotalPost = async () => {

    return request.get(
        'posts/total', {
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Basic ' + access_token
            }
        }
    )
}
