import {
    restGetTotalUser,
} from '../rest/user';

export const totalUser = () => {
    return dispatch => {
        restGetTotalUser().then(result => {
            let {
                data
            } = result
            dispatch({
                type: 'TOTAL_REGISTERED_USERS_SUCCESS',
                data
            })
        }).catch(error => {
            dispatch({
                type: 'TOTAL_REGISTERED_USERS_ERROR',
                error
            })
            throw error
        })
    }
}
