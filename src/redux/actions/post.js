import {
    restGetTotalPost,
} from '../rest/post';

export const totalPost = () => {
    return dispatch => {
        restGetTotalPost().then(result => {
            let {
                data
            } = result
            dispatch({
                type: 'TOTAL_USERS_POSTS_SUCCESS',
                data
            })
        }).catch(error => {
            dispatch({
                type: 'TOTAL_USERS_POSTS_ERROR',
                error
            })
            throw error
        })
    }
}
