import * as UserActions from './user';
import * as PostActions from './post';

export const ActionCreators = Object.assign({},
    UserActions,
    PostActions,
);