import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { ActionCreators } from '../redux/actions'

import { StatsCard } from "components/StatsCard/StatsCard.jsx";

class TotalUser extends Component {

  constructor(props) {
    super(props)

    this.state = {
      data: null
    }
  }
  componentDidMount () {
    let { totalUser } = this.props;

    totalUser();
  }

  componentWillReceiveProps(nextProps) {
    let { userData } = nextProps;

    if (userData.type === 'TOTAL_REGISTERED_USERS_SUCCESS') {
      this.setState({data:userData.data})
    }
  }

  render() {
    return (
      <StatsCard
        bigIcon={<i className="pe-7s-users text-success" />}
        statsText="Total Users"
        statsValue={this.state.data !== null ? this.state.data.data : '0'}
        statsIcon={<i className="fa fa-calendar-o" />}
        statsIconText="Last day"
      />
    );
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(ActionCreators, dispatch);
}

const mapStateToProps = state => {
  return {
    userData: state.getRegisterData,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TotalUser);