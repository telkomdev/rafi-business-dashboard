import React, { Component } from "react";

import { StatsCard } from "components/StatsCard/StatsCard.jsx";

class TotalDownload extends Component {
  render() {
    return (
      <StatsCard
        bigIcon={<i className="pe-7s-download text-danger" />}
        statsText="Download"
        statsValue="9364"
        statsIcon={<i className="fa fa-clock-o" />}
        statsIconText="In the last hour"
      />
    );
  }
}

export default TotalDownload;