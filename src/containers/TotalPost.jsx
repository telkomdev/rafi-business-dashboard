import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { ActionCreators } from '../redux/actions'

import { StatsCard } from "components/StatsCard/StatsCard.jsx";

class TotalPost extends Component {

  constructor(props) {
    super(props)

    this.state = {
      data: null
    }
  }
  componentDidMount() {
    let { totalPost } = this.props;

    totalPost();
  }

  componentWillReceiveProps(nextProps) {
    let { postData } = nextProps;

    if (postData.type === 'TOTAL_USERS_POSTS_SUCCESS') {
      this.setState({ data: postData.data })
    }
  }

  render() {
    return (
      <StatsCard
        bigIcon={<i className="pe-7s-note2 text-warning" />}
        statsText="Total Post"
        statsValue={this.state.data !== null ? this.state.data.data : '0'}
        statsIcon={<i className="fa fa-refresh" />}
        statsIconText="Updated now"
      />
    );
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(ActionCreators, dispatch);
}

const mapStateToProps = state => {
  return {
    postData: state.getPostData,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TotalPost);