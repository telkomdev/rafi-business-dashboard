import React from "react";
import ReactDOM from "react-dom";

import {
  HashRouter,
  Switch
} from "react-router-dom";
import {
  Route,
} from "react-router";
import {
  createStore,
  combineReducers,
  applyMiddleware
} from 'redux';
import {
  ConnectedRouter,
  routerReducer,
  routerMiddleware,
  push
} from 'react-router-redux';
import { Provider } from 'react-redux';
import createHistory from 'history/createBrowserHistory';

import indexRoutes from "routes/index.jsx";
import App from 'App';

import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/animate.min.css";
import "./assets/sass/light-bootstrap-dashboard.css?v=1.2.0";
import "./assets/css/demo.css";
import "./assets/css/pe-icon-7-stroke.css";

const history = createHistory();
const middleware = routerMiddleware(history)
const store = createStore(
  combineReducers({
    router: routerReducer
  }),
  applyMiddleware(middleware)
);
/*ReactDOM.render( 
  <Provider store = {
    store
  } >
  <ConnectedRouter history = {
    history
  } >
    <div>
   {
    indexRoutes.map((prop, key) => {
      return <Route to = {
        prop.path
      }
      component = {
        prop.component
      }
      key = {
        key
      }
      />;
    })
  }
  </div>
  </ConnectedRouter>
  </Provider>,
  document.getElementById("root")
);
*/
ReactDOM.render(
  <App />,
  document.getElementById("root")
);