//
// //
// // // For notifications
// //
//
var defaultWidth =
  window.screen.width > 768 ? window.screen.width * 1 / 3 : window.screen.width;

var style = {
  Wrapper: {},
  Containers: {
    DefaultStyle: {
      position: "fixed",
      width: defaultWidth,
      padding: "10px 10px 10px 20px",
      zIndex: 9998,
      WebkitBoxSizing: "",
      MozBoxSizing: "",
      boxSizing: "",
      height: "auto",
      display: "inline-block",
      border: "0",
      fontSize: "14px",
      WebkitFontSmoothing: "antialiased",
      fontFamily: '"Roboto","Helvetica Neue",Arial,sans-serif',
      fontWeight: "400",
      color: "#FFFFFF"
    },

    tl: {
      top: "0px",
      bottom: "auto",
      left: "0px",
      right: "auto"
    },

    tr: {
      top: "0px",
      bottom: "auto",
      left: "auto",
      right: "0px"
    },

    tc: {
      top: "0px",
      bottom: "auto",
      margin: "0 auto",
      left: "50%",
      marginLeft: -(defaultWidth / 2)
    },

    bl: {
      top: "auto",
      bottom: "0px",
      left: "0px",
      right: "auto"
    },

    br: {
      top: "auto",
      bottom: "0px",
      left: "auto",
      right: "0px"
    },

    bc: {
      top: "auto",
      bottom: "0px",
      margin: "0 auto",
      left: "50%",
      marginLeft: -(defaultWidth / 2)
    }
  },

  NotificationItem: {
    DefaultStyle: {
      position: "relative",
      width: "100%",
      cursor: "pointer",
      borderRadius: "4px",
      fontSize: "14px",
      margin: "10px 0 0",
      padding: "10px",
      display: "block",
      WebkitBoxSizing: "border-box",
      MozBoxSizing: "border-box",
      boxSizing: "border-box",
      opacity: 0,
      transition: "all 0.5s ease-in-out",
      WebkitTransform: "translate3d(0, 0, 0)",
      transform: "translate3d(0, 0, 0)",
      willChange: "transform, opacity",

      isHidden: {
        opacity: 0
      },

      isVisible: {
        opacity: 1
      }
    },

    success: {
      borderTop: 0,
      backgroundColor: "#a1e82c",
      WebkitBoxShadow: 0,
      MozBoxShadow: 0,
      boxShadow: 0
    },

    error: {
      borderTop: 0,
      backgroundColor: "#fc727a",
      WebkitBoxShadow: 0,
      MozBoxShadow: 0,
      boxShadow: 0
    },

    warning: {
      borderTop: 0,
      backgroundColor: "#ffbc67",
      WebkitBoxShadow: 0,
      MozBoxShadow: 0,
      boxShadow: 0
    },

    info: {
      borderTop: 0,
      backgroundColor: "#63d8f1",
      WebkitBoxShadow: 0,
      MozBoxShadow: 0,
      boxShadow: 0
    }
  },

  Title: {
    DefaultStyle: {
      fontSize: "30px",
      margin: "0",
      padding: 0,
      fontWeight: "bold",
      color: "#FFFFFF",
      display: "block",
      left: "15px",
      position: "absolute",
      top: "50%",
      marginTop: "-15px"
    }
  },

  MessageWrapper: {
    DefaultStyle: {
      marginLeft: "55px",
      marginRight: "30px",
      padding: "0 12px 0 0",
      color: "#FFFFFF",
      maxWidthwidth: "89%"
    }
  },

  Dismiss: {
    DefaultStyle: {
      fontFamily: "inherit",
      fontSize: "21px",
      color: "#000",
      float: "right",
      position: "absolute",
      right: "10px",
      top: "50%",
      marginTop: "-13px",
      backgroundColor: "#FFFFFF",
      display: "block",
      borderRadius: "50%",
      opacity: ".4",
      lineHeight: "11px",
      width: "25px",
      height: "25px",
      outline: "0 !important",
      textAlign: "center",
      padding: "6px 3px 3px 3px",
      fontWeight: "300",
      marginLeft: "65px"
    },

    success: {
      // color: '#f0f5ea',
      // backgroundColor: '#a1e82c'
    },

    error: {
      // color: '#f4e9e9',
      // backgroundColor: '#fc727a'
    },

    warning: {
      // color: '#f9f6f0',
      // backgroundColor: '#ffbc67'
    },

    info: {
      // color: '#e8f0f4',
      // backgroundColor: '#63d8f1'
    }
  },

  Action: {
    DefaultStyle: {
      background: "#ffffff",
      borderRadius: "2px",
      padding: "6px 20px",
      fontWeight: "bold",
      margin: "10px 0 0 0",
      border: 0
    },

    success: {
      backgroundColor: "#a1e82c",
      color: "#ffffff"
    },

    error: {
      backgroundColor: "#fc727a",
      color: "#ffffff"
    },

    warning: {
      backgroundColor: "#ffbc67",
      color: "#ffffff"
    },

    info: {
      backgroundColor: "#63d8f1",
      color: "#ffffff"
    }
  },

  ActionWrapper: {
    DefaultStyle: {
      margin: 0,
      padding: 0
    }
  }
};

//
// //
// // // For tables
// //
//
const thArray = ["ID", "Name", "Salary", "Country", "City"];
const tdArray = [
  ["1", "Dakota Rice", "$36,738", "Niger", "Oud-Turnhout"],
  ["2", "Minerva Hooper", "$23,789", "Curaçao", "Sinaai-Waas"],
  ["3", "Sage Rodriguez", "$56,142", "Netherlands", "Baileux"],
  ["4", "Philip Chaney", "$38,735", "Korea, South", "Overland Park"],
  ["5", "Doris Greene", "$63,542", "Malawi", "Feldkirchen in Kärnten"],
  ["6", "Mason Porter", "$78,615", "Chile", "Gloucester"]
];

//
// //
// // // For icons
// //
//
const iconsArray = [
  "pe-7s-album",
  "pe-7s-arc",
  "pe-7s-back-2",
  "pe-7s-bandaid",
  "pe-7s-car",
  "pe-7s-diamond",
  "pe-7s-door-lock",
  "pe-7s-eyedropper",
  "pe-7s-female",
  "pe-7s-gym",
  "pe-7s-hammer",
  "pe-7s-headphones",
  "pe-7s-helm",
  "pe-7s-hourglass",
  "pe-7s-leaf",
  "pe-7s-magic-wand",
  "pe-7s-male",
  "pe-7s-map-2",
  "pe-7s-next-2",
  "pe-7s-paint-bucket",
  "pe-7s-pendrive",
  "pe-7s-photo",
  "pe-7s-piggy",
  "pe-7s-plugin",
  "pe-7s-refresh-2",
  "pe-7s-rocket",
  "pe-7s-settings",
  "pe-7s-shield",
  "pe-7s-smile",
  "pe-7s-usb",
  "pe-7s-vector",
  "pe-7s-wine",
  "pe-7s-cloud-upload",
  "pe-7s-cash",
  "pe-7s-close",
  "pe-7s-bluetooth",
  "pe-7s-cloud-download",
  "pe-7s-way",
  "pe-7s-close-circle",
  "pe-7s-id",
  "pe-7s-angle-up",
  "pe-7s-wristwatch",
  "pe-7s-angle-up-circle",
  "pe-7s-world",
  "pe-7s-angle-right",
  "pe-7s-volume",
  "pe-7s-angle-right-circle",
  "pe-7s-users",
  "pe-7s-angle-left",
  "pe-7s-user-female",
  "pe-7s-angle-left-circle",
  "pe-7s-up-arrow",
  "pe-7s-angle-down",
  "pe-7s-switch",
  "pe-7s-angle-down-circle",
  "pe-7s-scissors",
  "pe-7s-wallet",
  "pe-7s-safe",
  "pe-7s-volume2",
  "pe-7s-volume1",
  "pe-7s-voicemail",
  "pe-7s-video",
  "pe-7s-user",
  "pe-7s-upload",
  "pe-7s-unlock",
  "pe-7s-umbrella",
  "pe-7s-trash",
  "pe-7s-tools",
  "pe-7s-timer",
  "pe-7s-ticket",
  "pe-7s-target",
  "pe-7s-sun",
  "pe-7s-study",
  "pe-7s-stopwatch",
  "pe-7s-star",
  "pe-7s-speaker",
  "pe-7s-signal",
  "pe-7s-shuffle",
  "pe-7s-shopbag",
  "pe-7s-share",
  "pe-7s-server",
  "pe-7s-search",
  "pe-7s-film",
  "pe-7s-science",
  "pe-7s-disk",
  "pe-7s-ribbon",
  "pe-7s-repeat",
  "pe-7s-refresh",
  "pe-7s-add-user",
  "pe-7s-refresh-cloud",
  "pe-7s-paperclip",
  "pe-7s-radio",
  "pe-7s-note2",
  "pe-7s-print",
  "pe-7s-network",
  "pe-7s-prev",
  "pe-7s-mute",
  "pe-7s-power",
  "pe-7s-medal",
  "pe-7s-portfolio",
  "pe-7s-like2",
  "pe-7s-plus",
  "pe-7s-left-arrow",
  "pe-7s-play",
  "pe-7s-key",
  "pe-7s-plane",
  "pe-7s-joy",
  "pe-7s-photo-gallery",
  "pe-7s-pin",
  "pe-7s-phone",
  "pe-7s-plug",
  "pe-7s-pen",
  "pe-7s-right-arrow",
  "pe-7s-paper-plane",
  "pe-7s-delete-user",
  "pe-7s-paint",
  "pe-7s-bottom-arrow",
  "pe-7s-notebook",
  "pe-7s-note",
  "pe-7s-next",
  "pe-7s-news-paper",
  "pe-7s-musiclist",
  "pe-7s-music",
  "pe-7s-mouse",
  "pe-7s-more",
  "pe-7s-moon",
  "pe-7s-monitor",
  "pe-7s-micro",
  "pe-7s-menu",
  "pe-7s-map",
  "pe-7s-map-marker",
  "pe-7s-mail",
  "pe-7s-mail-open",
  "pe-7s-mail-open-file",
  "pe-7s-magnet",
  "pe-7s-loop",
  "pe-7s-look",
  "pe-7s-lock",
  "pe-7s-lintern",
  "pe-7s-link",
  "pe-7s-like",
  "pe-7s-light",
  "pe-7s-less",
  "pe-7s-keypad",
  "pe-7s-junk",
  "pe-7s-info",
  "pe-7s-home",
  "pe-7s-help2",
  "pe-7s-help1",
  "pe-7s-graph3",
  "pe-7s-graph2",
  "pe-7s-graph1",
  "pe-7s-graph",
  "pe-7s-global",
  "pe-7s-gleam",
  "pe-7s-glasses",
  "pe-7s-gift",
  "pe-7s-folder",
  "pe-7s-flag",
  "pe-7s-filter",
  "pe-7s-file",
  "pe-7s-expand1",
  "pe-7s-exapnd2",
  "pe-7s-edit",
  "pe-7s-drop",
  "pe-7s-drawer",
  "pe-7s-download",
  "pe-7s-display2",
  "pe-7s-display1",
  "pe-7s-diskette",
  "pe-7s-date",
  "pe-7s-cup",
  "pe-7s-culture",
  "pe-7s-crop",
  "pe-7s-credit",
  "pe-7s-copy-file",
  "pe-7s-config",
  "pe-7s-compass",
  "pe-7s-comment",
  "pe-7s-coffee",
  "pe-7s-cloud",
  "pe-7s-clock",
  "pe-7s-check",
  "pe-7s-chat",
  "pe-7s-cart",
  "pe-7s-camera",
  "pe-7s-call",
  "pe-7s-calculator",
  "pe-7s-browser",
  "pe-7s-box2",
  "pe-7s-box1",
  "pe-7s-bookmarks",
  "pe-7s-bicycle",
  "pe-7s-bell",
  "pe-7s-battery",
  "pe-7s-ball",
  "pe-7s-back",
  "pe-7s-attention",
  "pe-7s-anchor",
  "pe-7s-albums",
  "pe-7s-alarm",
  "pe-7s-airplay"
];

//
// //
// // // // For dashboard's charts
// //
//
// Data for Pie Chart
var dataPie = {
  labels: ["40%", "20%", "40%"],
  series: [40, 20, 40]
};
var legendPie = {
  names: ["Open", "Bounce", "Unsubscribe"],
  types: ["info", "danger", "warning"]
};

// Data for Line Chart
var dataSales = {
  labels: [
    "9:00AM",
    "12:00AM",
    "3:00PM",
    "6:00PM",
    "9:00PM",
    "12:00PM",
    "3:00AM",
    "6:00AM"
  ],
  series: [
    [287, 385, 490, 492, 554, 586, 698, 695],
    [67, 152, 143, 240, 287, 335, 435, 437],
    [23, 113, 67, 108, 190, 239, 307, 308]
  ]
};
var optionsSales = {
  low: 0,
  high: 800,
  showArea: false,
  height: "245px",
  axisX: {
    showGrid: false
  },
  lineSmooth: true,
  showLine: true,
  showPoint: true,
  fullWidth: true,
  chartPadding: {
    right: 50
  }
};
var responsiveSales = [
  [
    "screen and (max-width: 640px)",
    {
      axisX: {
        labelInterpolationFnc: function(value) {
          return value[0];
        }
      }
    }
  ]
];
var legendSales = {
  names: ["Open", "Click", "Click Second Time"],
  types: ["info", "danger", "warning"]
};

// Data for Bar Chart
var dataBar = {
  labels: [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "Mai",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ],
  series: [
    [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895],
    [412, 243, 280, 580, 453, 353, 300, 364, 368, 410, 636, 695]
  ]
};
var optionsBar = {
  seriesBarDistance: 10,
  axisX: {
    showGrid: false
  },
  height: "245px"
};
var responsiveBar = [
  [
    "screen and (max-width: 640px)",
    {
      seriesBarDistance: 5,
      axisX: {
        labelInterpolationFnc: function(value) {
          return value[0];
        }
      }
    }
  ]
];
var legendBar = {
  names: ["Tesla Model S", "BMW 5 Series"],
  types: ["info", "danger"]
};

var activeUsersIos = [
  {
    "id": "Monthly",
    "color": "hsl(256, 70%, 50%)",
    "data": [
      {
        "x": "0",
        "y": 99
      },
      {
        "x": "1",
        "y": 109
      },
      {
        "x": "2",
        "y": 116
      },
      {
        "x": "3",
        "y": 124
      },
      {
        "x": "4",
        "y": 128
      },
      {
        "x": "5",
        "y": 140
      },
      {
        "x": "6",
        "y": 141
      },
      {
        "x": "7",
        "y": 141
      },
      {
        "x": "8",
        "y": 154
      },
      {
        "x": "9",
        "y": 158
      },
      {
        "x": "10",
        "y": 162
      },
      {
        "x": "11",
        "y": 167
      },
      {
        "x": "12",
        "y": 171
      },
      {
        "x": "13",
        "y": 175
      },
      {
        "x": "14",
        "y": 173
      },
      {
        "x": "15",
        "y": 178
      },
      {
        "x": "16",
        "y": 179
      },
      {
        "x": "17",
        "y": 194
      },
      {
        "x": "18",
        "y": 213
      },
      {
        "x": "19",
        "y": 215
      },
      {
        "x": "20",
        "y": 218
      },
      {
        "x": "21",
        "y": 219
      },
      {
        "x": "22",
        "y": 228
      },
      {
        "x": "23",
        "y": 244
      },
      {
        "x": "24",
        "y": 246
      },
      {
        "x": "25",
        "y": 257
      },
      {
        "x": "26",
        "y": 273
      },
      {
        "x": "27",
        "y": 284
      },
      {
        "x": "28",
        "y": 298
      },
      {
        "x": "29",
        "y": 301
      },
    ]
  },
  {
    "id": "Weekly",
    "color": "hsl(140, 70%, 50%)",
    "data": [
      {
        "x": "0",
        "y": 30
      },
      {
        "x": "1",
        "y": 37
      },
      {
        "x": "2",
        "y": 36
      },
      {
        "x": "3",
        "y": 43
      },
      {
        "x": "4",
        "y": 47
      },
      {
        "x": "5",
        "y": 60
      },
      {
        "x": "6",
        "y": 59
      },
      {
        "x": "7",
        "y": 61
      },
      {
        "x": "8",
        "y": 69
      },
      {
        "x": "9",
        "y": 66
      },
      {
        "x": "10",
        "y": 61
      },
      {
        "x": "11",
        "y": 67
      },
      {
        "x": "12",
        "y": 59
      },
      {
        "x": "13",
        "y": 61
      },
      {
        "x": "14",
        "y": 59
      },
      {
        "x": "15",
        "y": 52
      },
      {
        "x": "16",
        "y": 50
      },
      {
        "x": "17",
        "y": 74
      },
      {
        "x": "18",
        "y": 102
      },
      {
        "x": "19",
        "y": 115
      },
      {
        "x": "20",
        "y": 120
      },
      {
        "x": "21",
        "y": 123
      },
      {
        "x": "22",
        "y": 127
      },
      {
        "x": "23",
        "y": 143
      },
      {
        "x": "24",
        "y": 133
      },
      {
        "x": "25",
        "y": 129
      },
      {
        "x": "26",
        "y": 120
      },
      {
        "x": "27",
        "y": 120
      },
      {
        "x": "28",
        "y": 134
      },
      {
        "x": "29",
        "y": 129
      },
    ]
  },
  {
    "id": "Daily",
    "color": "hsl(179, 70%, 50%)",
    "data": [
      {
        "x": "0",
        "y": 4
      },
      {
        "x": "1",
        "y": 16
      },
      {
        "x": "2",
        "y": 17
      },
      {
        "x": "3",
        "y": 19
      },
      {
        "x": "4",
        "y": 14
      },
      {
        "x": "5",
        "y": 25
      },
      {
        "x": "6",
        "y": 10
      },
      {
        "x": "7",
        "y": 10
      },
      {
        "x": "8",
        "y": 22
      },
      {
        "x": "9",
        "y": 15
      },
      {
        "x": "10",
        "y": 16
      },
      {
        "x": "11",
        "y": 21
      },
      {
        "x": "12",
        "y": 19
      },
      {
        "x": "13",
        "y": 13
      },
      {
        "x": "14",
        "y": 6
      },
      {
        "x": "15",
        "y": 14
      },
      {
        "x": "16",
        "y": 8
      },
      {
        "x": "17",
        "y": 42
      },
      {
        "x": "18",
        "y": 49
      },
      {
        "x": "19",
        "y": 48
      },
      {
        "x": "20",
        "y": 28
      },
      {
        "x": "21",
        "y": 18
      },
      {
        "x": "22",
        "y": 26
      },
      {
        "x": "23",
        "y": 32
      },
      {
        "x": "24",
        "y": 19
      },
      {
        "x": "25",
        "y": 30
      },
      {
        "x": "26",
        "y": 39
      },
      {
        "x": "27",
        "y": 31
      },
      {
        "x": "28",
        "y": 40
      },
      {
        "x": "29",
        "y": 26
      },
    ]
  }
];

var activeUsersAndroid = [
  {
    "id": "Monthly",
    "color": "hsl(256, 70%, 50%)",
    "data": [
      {
        "x": "0",
        "y": 686
      },
      {
        "x": "1",
        "y": 734
      },
      {
        "x": "2",
        "y": 750
      },
      {
        "x": "3",
        "y": 1011
      },
      {
        "x": "4",
        "y": 1167
      },
      {
        "x": "5",
        "y": 1283
      },
      {
        "x": "6",
        "y": 1408
      },
      {
        "x": "7",
        "y": 1483
      },
      {
        "x": "8",
        "y": 1616
      },
      {
        "x": "9",
        "y": 1701
      },
      {
        "x": "10",
        "y": 1722
      },
      {
        "x": "11",
        "y": 1765
      },
      {
        "x": "12",
        "y": 1838
      },
      {
        "x": "13",
        "y": 1862
      },
      {
        "x": "14",
        "y": 1905
      },
      {
        "x": "15",
        "y": 1982
      },
      {
        "x": "16",
        "y": 2021
      },
      {
        "x": "17",
        "y": 2127
      },
      {
        "x": "18",
        "y": 2237
      },
      {
        "x": "19",
        "y": 2326
      },
      {
        "x": "20",
        "y": 2397
      },
      {
        "x": "21",
        "y": 2429
      },
      {
        "x": "22",
        "y": 2485
      },
      {
        "x": "23",
        "y": 2570
      },
      {
        "x": "24",
        "y": 2619
      },
      {
        "x": "25",
        "y": 2753
      },
      {
        "x": "26",
        "y": 2892
      },
      {
        "x": "27",
        "y": 2985
      },
      {
        "x": "28",
        "y": 3186
      },
      {
        "x": "29",
        "y": 3280
      },
    ],   
  }, 
  {
    "id": "Weekly",
    "color": "hsl(260, 70%, 50%)",
    "data": [
      {
        "x": "0",
        "y": 209
      },
      {
        "x": "1",
        "y": 230
      },
      {
        "x": "2",
        "y": 222
      },
      {
        "x": "3",
        "y": 454
      },
      {
        "x": "4",
        "y": 605
      },
      {
        "x": "5",
        "y": 700
      },
      {
        "x": "6",
        "y": 829
      },
      {
        "x": "7",
        "y": 909
      },
      {
        "x": "8",
        "y": 1004
      },
      {
        "x": "9",
        "y": 1072
      },
      {
        "x": "10",
        "y": 883
      },
      {
        "x": "11",
        "y": 806
      },
      {
        "x": "12",
        "y": 805
      },
      {
        "x": "13",
        "y": 740
      },
      {
        "x": "14",
        "y": 743
      },
      {
        "x": "15",
        "y": 660
      },
      {
        "x": "16",
        "y": 629
      },
      {
        "x": "17",
        "y": 709
      },
      {
        "x": "18",
        "y": 778
      },
      {
        "x": "19",
        "y": 833
      },
      {
        "x": "20",
        "y": 875
      },
      {
        "x": "21",
        "y": 854
      },
      {
        "x": "22",
        "y": 832
      },
      {
        "x": "23",
        "y": 868
      },
      {
        "x": "24",
        "y": 841
      },
      {
        "x": "25",
        "y": 891
      },
      {
        "x": "26",
        "y": 926
      },
      {
        "x": "27",
        "y": 922
      },
      {
        "x": "28",
        "y": 1111
      },
      {
        "x": "29",
        "y": 1148
      },
    ]
  },
  {
    "id": "Daily",
    "color": "hsl(299, 70%, 50%)",
    "data": [
      {
        "x": "0",
        "y": 44
      },
      {
        "x": "1",
        "y": 102
      },
      {
        "x": "2",
        "y": 54
      },
      {
        "x": "3",
        "y": 55
      },
      {
        "x": "4",
        "y": 23
      },
      {
        "x": "5",
        "y": 22
      },
      {
        "x": "6",
        "y": 59
      },
      {
        "x": "7",
        "y": 57
      },
      {
        "x": "8",
        "y": 71
      },
      {
        "x": "9",
        "y": 31
      },
      {
        "x": "10",
        "y": 49
      },
      {
        "x": "11",
        "y": 16
      },
      {
        "x": "12",
        "y": 17
      },
      {
        "x": "13",
        "y": 83
      },
      {
        "x": "14",
        "y": 44
      },
      {
        "x": "15",
        "y": 292
      },
      {
        "x": "16",
        "y": 198
      },
      {
        "x": "17",
        "y": 156
      },
      {
        "x": "18",
        "y": 157
      },
      {
        "x": "19",
        "y": 111
      },
      {
        "x": "20",
        "y": 237
      },
      {
        "x": "21",
        "y": 136
      },
      {
        "x": "22",
        "y": 92
      },
      {
        "x": "23",
        "y": 118
      },
      {
        "x": "24",
        "y": 146
      },
      {
        "x": "25",
        "y": 79
      },
      {
        "x": "26",
        "y": 97
      },
      {
        "x": "27",
        "y": 126
      },
      {
        "x": "28",
        "y": 82
      },
      {
        "x": "29",
        "y": 185
      },
    ]
  }
];
var drink = [
  {
    "id": "whisky",
    "color": "hsl(256, 70%, 50%)",
    "data": [
      {
        "color": "hsl(83, 70%, 50%)",
        "x": "RE",
        "y": 7
      },
      {
        "color": "hsl(59, 70%, 50%)",
        "x": "MS",
        "y": 29
      },
      {
        "color": "hsl(217, 70%, 50%)",
        "x": "LT",
        "y": 28
      },
      {
        "color": "hsl(206, 70%, 50%)",
        "x": "MP",
        "y": 0
      },
      {
        "color": "hsl(281, 70%, 50%)",
        "x": "AG",
        "y": 9
      },
      {
        "color": "hsl(301, 70%, 50%)",
        "x": "TF",
        "y": 47
      },
      {
        "color": "hsl(329, 70%, 50%)",
        "x": "ST",
        "y": 39
      },
      {
        "color": "hsl(146, 70%, 50%)",
        "x": "MK",
        "y": 11
      },
      {
        "color": "hsl(227, 70%, 50%)",
        "x": "BA",
        "y": 50
      }
    ]
  },
  {
    "id": "rhum",
    "color": "hsl(179, 70%, 50%)",
    "data": [
      {
        "color": "hsl(242, 70%, 50%)",
        "x": "RE",
        "y": 40
      },
      {
        "color": "hsl(249, 70%, 50%)",
        "x": "MS",
        "y": 30
      },
      {
        "color": "hsl(10, 70%, 50%)",
        "x": "LT",
        "y": 54
      },
      {
        "color": "hsl(164, 70%, 50%)",
        "x": "MP",
        "y": 11
      },
      {
        "color": "hsl(310, 70%, 50%)",
        "x": "AG",
        "y": 12
      },
      {
        "color": "hsl(355, 70%, 50%)",
        "x": "TF",
        "y": 19
      },
      {
        "color": "hsl(85, 70%, 50%)",
        "x": "ST",
        "y": 46
      },
      {
        "color": "hsl(321, 70%, 50%)",
        "x": "MK",
        "y": 26
      },
      {
        "color": "hsl(59, 70%, 50%)",
        "x": "BA",
        "y": 53
      }
    ]
  },
  {
    "id": "gin",
    "color": "hsl(299, 70%, 50%)",
    "data": [
      {
        "color": "hsl(87, 70%, 50%)",
        "x": "RE",
        "y": 4
      },
      {
        "color": "hsl(195, 70%, 50%)",
        "x": "MS",
        "y": 21
      },
      {
        "color": "hsl(39, 70%, 50%)",
        "x": "LT",
        "y": 45
      },
      {
        "color": "hsl(131, 70%, 50%)",
        "x": "MP",
        "y": 31
      },
      {
        "color": "hsl(221, 70%, 50%)",
        "x": "AG",
        "y": 50
      },
      {
        "color": "hsl(153, 70%, 50%)",
        "x": "TF",
        "y": 50
      },
      {
        "color": "hsl(348, 70%, 50%)",
        "x": "ST",
        "y": 0
      },
      {
        "color": "hsl(272, 70%, 50%)",
        "x": "MK",
        "y": 31
      },
      {
        "color": "hsl(146, 70%, 50%)",
        "x": "BA",
        "y": 22
      }
    ]
  },
  {
    "id": "vodka",
    "color": "hsl(140, 70%, 50%)",
    "data": [
      {
        "color": "hsl(58, 70%, 50%)",
        "x": "RE",
        "y": 23
      },
      {
        "color": "hsl(233, 70%, 50%)",
        "x": "MS",
        "y": 12
      },
      {
        "color": "hsl(17, 70%, 50%)",
        "x": "LT",
        "y": 18
      },
      {
        "color": "hsl(209, 70%, 50%)",
        "x": "MP",
        "y": 26
      },
      {
        "color": "hsl(83, 70%, 50%)",
        "x": "AG",
        "y": 49
      },
      {
        "color": "hsl(106, 70%, 50%)",
        "x": "TF",
        "y": 22
      },
      {
        "color": "hsl(224, 70%, 50%)",
        "x": "ST",
        "y": 6
      },
      {
        "color": "hsl(343, 70%, 50%)",
        "x": "MK",
        "y": 15
      },
      {
        "color": "hsl(94, 70%, 50%)",
        "x": "BA",
        "y": 1
      }
    ]
  },
  {
    "id": "cognac",
    "color": "hsl(260, 70%, 50%)",
    "data": [
      {
        "color": "hsl(263, 70%, 50%)",
        "x": "RE",
        "y": 20
      },
      {
        "color": "hsl(326, 70%, 50%)",
        "x": "MS",
        "y": 44
      },
      {
        "color": "hsl(308, 70%, 50%)",
        "x": "LT",
        "y": 40
      },
      {
        "color": "hsl(43, 70%, 50%)",
        "x": "MP",
        "y": 0
      },
      {
        "color": "hsl(14, 70%, 50%)",
        "x": "AG",
        "y": 26
      },
      {
        "color": "hsl(343, 70%, 50%)",
        "x": "TF",
        "y": 3
      },
      {
        "color": "hsl(343, 70%, 50%)",
        "x": "ST",
        "y": 3
      },
      {
        "color": "hsl(279, 70%, 50%)",
        "x": "MK",
        "y": 29
      },
      {
        "color": "hsl(275, 70%, 50%)",
        "x": "BA",
        "y": 33
      }
    ]
  }
]

module.exports = {
  style, // For notifications (App container and Notifications view)
  thArray,
  tdArray, // For tables (TableList view)
  iconsArray, // For icons (Icons view)
  dataPie,
  legendPie,
  dataSales,
  optionsSales,
  responsiveSales,
  legendSales,
  dataBar,
  optionsBar,
  responsiveBar,
  activeUsersAndroid,
  activeUsersIos,
  drink,
  legendBar // For charts (Dashboard view)
};
