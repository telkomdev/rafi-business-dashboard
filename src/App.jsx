import React, { Component } from "react";
import { Router, Switch } from "react-router-dom";
import { Route } from "react-router";
import { Provider } from 'react-redux'
import { history, store } from 'redux/store'

import indexRoutes from "routes/index.jsx";

const App = () => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Switch>
          {indexRoutes.map((prop, key) => {
            return <Route to={prop.path} component={prop.component} key={key} />;
          })}
        </Switch>
      </Router>
    </Provider>
  );
}
export default App;